# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]

## [1.0.0]

Initial Release!

### Added

- Aerolite 103 Full Checklist + Cabin Tour

[Unreleased]: https://gitlab.com/Gneu/x-plane/checklist/compare/master...develop
[1.0.0]: https://gitlab.com/Gneu/x-plane/checklist/tags/1.0.0
