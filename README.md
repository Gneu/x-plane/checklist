# XChecklist Inventory

[XChecklist](https://github.com/sparker256/xchecklist) is a tool for X-Plane which provides a semi-automated checklist to help with your piloting needs, driven by text based scripts. 

## Setup & Configuration

1. Install [XChecklist](https://github.com/sparker256/xchecklist/releases)
2. [Download the latest release](https://gitlab.com/Gneu/x-plane/checklist/tags) of this repo
3. Unzip the contents into your X-Plane 11 directory
4. Enjoy

## Current Inventory

* Aerolite 103 - Full Checklist + Cabin Tour

## Contributing

I am definitely interested in any assistance. 

The checklists here are largely driven by actions in the game, as I prefer to avoid checking elements manually while also acting in the simulator. As a policy this is likely to be maintained going forward. 

## License

Attribution-ShareAlike 4.0 International
